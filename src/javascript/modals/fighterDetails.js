import {createElement} from '../helpers/domHelper';
import {showModal} from './modal';

export function showFighterDetailsModal(fighter) {
    const title = 'Fighter Specifications';
    const bodyElement = createFighterDetails(fighter);
    showModal({title, bodyElement});
}

function createFighterDetails(fighter) {
    const {name, attack, defense, health, source} = fighter;

    const fighterDetails = createElement({tagName: 'div', className: 'modal-body'});
    const propsContainer = createElement({tagName: 'div', className: 'container'});
    const nameElement = createElement({tagName: 'h4'});
    const attackElement = createElement({tagName: 'h4'});
    const defenseElement = createElement({tagName: 'h4'});
    const healthElement = createElement({tagName: 'h4'});
    const imgElement = createElement({tagName: 'img', className: 'fighter-img-details', attributes: {src: source}});

    nameElement.innerText = `Name: ${name}\n\n`;
    attackElement.innerText = `Punch power: ${attack}\n\n`;
    defenseElement.innerText = `Defense abilities: ${defense}\n\n`;
    healthElement.innerText = `Health capacity: ${health}`;
    propsContainer.append(nameElement, attackElement, defenseElement, healthElement);
    fighterDetails.append(imgElement, propsContainer);

    return fighterDetails;
}
