import {createElement} from "../helpers/domHelper";
import {showModal} from "./modal";
import {createImage} from "../fighterView";

export function showWinnerModal(winner, loser, hits) {
    const title = 'Game Over';
    const {name: winnerName, source: winnerImg, health: winnerHealth} = winner;
    const {name: loserName, source: loserImg, health: loserHealth} = loser;


    const bodyElement = createElement({tagName: 'div', className: 'modal-body'});
    const nameElement = createElement({tagName: 'h2', attributes: {style: 'text-align:center;'}});
    const hitsElement = createElement({tagName: 'h2', attributes: {style: 'text-align:center;'}});
    const winnerHealthElement = createElement({tagName: 'h3', attributes: {style: 'text-align:center;'}});
    const loserHealthElement = createElement({tagName: 'h3', attributes: {style: 'text-align:center;'}});
    const winnerImgElement = createImage(winnerImg);
    const loserImgElement = createImage(loserImg);

    nameElement.innerText = `${winnerName} has Won!`;
    hitsElement.innerText = `It took ${hits} hits to Destroy the Opponent !`;
    winnerHealthElement.innerText = `${winnerName} has ${Math.floor(winnerHealth)}hp remaining`;
    loserHealthElement.innerText = `${loserName} died with ${Math.floor(loserHealth)}hp`;
    winnerImgElement.style.transform = "scale(0.85)";
    loserImgElement.style.transform = "rotateY(180deg) scale(0.85)";

    bodyElement.append(nameElement);
    bodyElement.append(hitsElement);
    bodyElement.append(winnerHealthElement);
    bodyElement.append(loserHealthElement);
    bodyElement.append(winnerImgElement);
    bodyElement.append(loserImgElement);
    bodyElement.style.textAlign = 'center';

    showModal({title, bodyElement});
}

