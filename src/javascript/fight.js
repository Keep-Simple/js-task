
export function fight(firstFighter, secondFighter) {

    let firstCopy = {...firstFighter};
    let secondCopy = {...secondFighter};

    let { winner, loser, hits } = turn(firstCopy, secondCopy, 0);

    return { winner, loser, hits };
}


function turn(attacker, target, hits){
    while (true) {
        if(attacker.health <= 0) return { winner: target, loser: attacker, hits: Math.ceil(hits/2) };

        hits++;
        target.health -= getDamage(attacker, target);
        [attacker, target] = [target, attacker];
    }
}


export function getDamage(attacker, enemy) {
    const damage = getHitPower(attacker) - getBlockPower(enemy);

    return (damage < 0) ? 0 : damage;
}


const oneToTwo = () => 1 + Math.random();


export function getHitPower(fighter) {

    return fighter.attack * oneToTwo();
}


export function getBlockPower(fighter) {

    return fighter.defense * oneToTwo();
}


